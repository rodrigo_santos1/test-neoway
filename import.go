package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

type entity struct {
	cpf                    string
	validCpf               bool
	private                int
	incomplete             int
	dateLastPurchase       time.Time
	averageTicket          float64
	lastPurchaseTicket     float64
	moreFrequentStore      string
	validMoreFrequentStore bool
	lastBuyStore           string
	validLastBuyStore      bool
}

func newEntity(fields []string) *entity {
	e := new(entity)
	e.cpf = clearCpfCnpj(fields[0])
	e.validCpf = validateCpf(e.cpf)
	e.private = strToInt(fields[1])
	e.incomplete = strToInt(fields[2])
	e.dateLastPurchase = strToTime("2006-01-02", fields[3])
	e.averageTicket = strToFloat(fields[4])
	e.lastPurchaseTicket = strToFloat(fields[5])
	e.moreFrequentStore = clearCpfCnpj(fields[6])
	e.validMoreFrequentStore = validateCnpj(e.moreFrequentStore)
	e.lastBuyStore = clearCpfCnpj(fields[7])
	e.validLastBuyStore = validateCnpj(e.lastBuyStore)
	return e
}

type database struct {
	db *sql.DB
}

func (d *database) connect(driverName, dataSourceName string) {
	if d.db, _ = sql.Open(driverName, dataSourceName); d.db.Ping() != nil {
		exit("failed to connect to the database")
	}
}

func (d *database) save(e *entity) error {
	_, err := d.db.Exec(`INSERT INTO entity (cpf, valid_cpf, private, incomplete, date_last_purchase, average_ticket, last_purchase_ticket, 
		more_frequent_store, valid_more_frequent_store, last_buy_store, valid_last_buy_store) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`,
		e.cpf, e.validCpf, e.private, e.incomplete, e.dateLastPurchase, e.averageTicket, e.lastPurchaseTicket,
		e.moreFrequentStore, e.validMoreFrequentStore, e.lastBuyStore, e.validLastBuyStore)
	return err
}

func main() {
	checkArguments()
	godotenv.Load()
	pg := new(database)
	pg.connect("postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("HOST"), os.Getenv("PORT"), os.Getenv("PG_USER"), os.Getenv("PG_PASSWORD"), os.Getenv("PG_DB")))
	defer pg.db.Close()
	for _, filename := range os.Args[1:] {
		fmt.Println(fmt.Sprintf("starting to read the %s file", filename))
		for index, slice := range strings.Split(readFile(filename), "\n")[1:] {
			fields := strings.Fields(slice)
			checkFields(index, fields)
			if err := pg.save(newEntity(fields)); err != nil && strings.Contains(err.Error(), "duplicate key") {
				fmt.Println(fmt.Sprintf("line %d already exists in the database", index+1))
			} else {
				fmt.Println(fmt.Sprintf("line %d successfully saved", index+1))
			}
		}
	}
}

func checkArguments() {
	if len(os.Args) < 2 {
		exit("no files found")
	}
}

func checkFields(index int, fields []string) {
	if len(fields) != 8 {
		exit(fmt.Sprintf("line %d is wrong in the file", index+1))
	}
}

func clearCpfCnpj(value string) string {
	return regexp.MustCompile(`\D`).ReplaceAllString(value, "")
}

func exit(message string) {
	if len(message) > 0 {
		fmt.Println(message)
	}
	os.Exit(2)
}

func readFile(filename string) string {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		exit(fmt.Sprintf("%s not found", filename))
	}
	if len(b) == 0 {
		exit(fmt.Sprintf("blank %s file", filename))
	}
	return string(b)
}

func strToFloat(value string) float64 {
	f, _ := strconv.ParseFloat(strings.ReplaceAll(value, ",", "."), 64)
	return f
}

func strToInt(value string) int {
	i, _ := strconv.Atoi(value)
	return i
}

func strToTime(layout, value string) time.Time {
	t, _ := time.Parse(layout, value)
	return t
}

func validateCnpj(value string) bool {
	if len(value) < 14 {
		return false
	}
	other := value[0:12]
	for _, sequence := range [][]int{{5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2}, {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2}} {
		summation := 0
		for index, multiplier := range sequence {
			summation += strToInt(value[index:index+1]) * multiplier
		}
		if result := summation % 11; result < 2 {
			other += "0"
		} else {
			other += strconv.Itoa(11 - result)
		}
	}
	return value == other
}

func validateCpf(value string) bool {
	if len(value) < 11 {
		return false
	}
	other := value[0:9]
	for _, sequence := range [][]int{{10, 9, 8, 7, 6, 5, 4, 3, 2}, {11, 10, 9, 8, 7, 6, 5, 4, 3, 2}} {
		summation := 0
		for index, multiplier := range sequence {
			summation += strToInt(value[index:index+1]) * multiplier
		}
		if result := summation * 10 % 11; result == 10 {
			other += "0"
		} else {
			other += strconv.Itoa(result)
		}
	}
	return value == other
}
