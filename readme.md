# Import

## Getting Started
Install [Git 2.25.0+](http://git-scm.com) and then, by the terminal, download the project using the command below:

```
git clone git@gitlab.com:rodrigo_santos1/test-neoway.git
```

Install [Go 1.13.6+](https://golang.org) and then, by the terminal, run the commands bellow to install the dependencies:

```
go get github.com/joho/godotenv
go get github.com/lib/pq
```

## Environment Variables
Create a file named [.env](.env) in the project root and add the environment variables below:

```
HOST=0.0.0.0
PORT=5432
PG_USER=postgres
PG_PASSWORD=12345
PG_DB=postgres
```

## Docker
Install [Docker 2.1.0.5+](https://www.docker.com) and then, by the terminal, access the project directory and execute the commands below:

```
docker-compose up --detach
```

## Running the Application
By the terminal, access the project directory and run the command below:

```
go run import.go test.csv
```

## Authors

**Rodrigo Santos**
