CREATE TABLE entity (
    cpf VARCHAR (11) PRIMARY KEY,
    valid_cpf BOOLEAN, 
    private SMALLINT,
    incomplete SMALLINT,
    date_last_purchase DATE,
    average_ticket MONEY,
    last_purchase_ticket MONEY,
	more_frequent_store VARCHAR (14),
    valid_more_frequent_store BOOLEAN,
    last_buy_store VARCHAR (14),
    valid_last_buy_store BOOLEAN
)
